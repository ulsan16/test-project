﻿using System;


namespace GoodCodeToBe
{
    public interface GoodCodeTo
    {
        decimal Calculate(decimal amount, int type, int years);

        void arcTo(double rx, double ry, double xAxisRotation, bool largeArcFlag, bool sweepFlag, double x, double y);
    }

    public class BezierPath
    {
        public void add(Node node)
        {

        }
    }

    public class Node
    {
        public int mask { get; set;}
        public int[] y { get; set;}
        public int[] x { get; set;}
    }

    public class Point2D
    {
        public int x { get; set; }
        
        public int y { get; set; }
        
        public int[] point { get; set; }
    }

    public class GoodCodeToBe : GoodCodeTo
    {
        public decimal Calculate(decimal amount, int type, int years)
        {
            decimal result = 0;
            decimal disc = (years > 5) ? (decimal) 5 / 100 : (decimal) years / 100;
            if (type == 1)
            {
                result = amount;
            }
            else if (type == 2)
            {
                result = (amount - (0.1m * amount)) - disc * (amount - (0.1m * amount));
            }
            else if (type == 3)
            {
                result = (0.7m * amount) - disc * (0.7m * amount);
            }
            else if (type == 4)
            {
                result = (amount - (0.5m * amount)) - disc * (amount - (0.5m * amount));
            }

            return result;
        }

        public void arcTo(double rx, double ry, double xAxisRotation, bool largeArcFlag, bool sweepFlag, double x, double y)
        {

            // Ensure radii are valid
            if (rx == 0 || ry == 0)
            {
                (new Point2D.Double(x, y));
                return;
            }

            // Get the current (x, y) coordinates of the path
            Node lastPoint = get(size() - 1);
            double x0 = ((lastPoint.mask & C2_MASK) == C2_MASK) ? lastPoint.x[2] : lastPoint.x[0];
            double y0 = ((lastPoint.mask & C2_MASK) == C2_MASK) ? lastPoint.y[2] : lastPoint.y[0];

            if (x0 == x && y0 == y)
            {
                // If the endpoints (x, y) and (x0, y0) are identical, then this
                // is equivalent to omitting the elliptical arc segment entirely.
                return;
            }

            // Compute the half distance between the current and the final point
            double dx2 = (x0 - x) / 2d;
            double dy2 = (y0 - y) / 2d;
            // Convert angle from degrees to radians
            double angle = Math.toRadians(xAxisRotation);
            double cosAngle = Math.Cos(angle);
            double sinAngle = Math.Sin(angle);
            Math.toRadians()
            //
            // Step 1 : Compute (x1, y1)
            //
            double x1 = (cosAngle * dx2 + sinAngle * dy2);
            double y1 = (-sinAngle * dx2 + cosAngle * dy2);
            // Ensure radii are large enough
            rx = Math.Abs(rx);
            ry = Math.Abs(ry);
            double Prx = rx * rx;
            double Pry = ry * ry;
            double Px1 = x1 * x1;
            double Py1 = y1 * y1;
            // check that radii are large enough
            double radiiCheck = Px1 / Prx + Py1 / Pry;
            if (radiiCheck > 1)
            {
                rx = Math.Sqrt(radiiCheck) * rx;
                ry = Math.Sqrt(radiiCheck) * ry;
                Prx = rx * rx;
                Pry = ry * ry;
            }

            //
            // Step 2 : Compute (cx1, cy1)
            //
            double sign = (largeArcFlag == sweepFlag) ? -1 : 1;
            double sq = ((Prx * Pry) - (Prx * Py1) - (Pry * Px1)) / ((Prx * Py1) + (Pry * Px1));
            sq = (sq < 0) ? 0 : sq;
            double coef = (sign * Math.sqrt(sq));
            double cx1 = coef * ((rx * y1) / ry);
            double cy1 = coef * -((ry * x1) / rx);

            //
            // Step 3 : Compute (cx, cy) from (cx1, cy1)
            //
            double sx2 = (x0 + x) / 2.0;
            double sy2 = (y0 + y) / 2.0;
            double cx = sx2 + (cosAngle * cx1 - sinAngle * cy1);
            double cy = sy2 + (sinAngle * cx1 + cosAngle * cy1);

            //
            // Step 4 : Compute the angleStart (angle1) and the angleExtent (dangle)
            //
            double ux = (x1 - cx1) / rx;
            double uy = (y1 - cy1) / ry;
            double vx = (-x1 - cx1) / rx;
            double vy = (-y1 - cy1) / ry;
            double p, n;

            // Compute the angle start
            n = Math.sqrt((ux * ux) + (uy * uy));
            p = ux; // (1 * ux) + (0 * uy)
            sign = (uy < 0) ? -1d : 1d;
            double angleStart = Math.toDegrees(sign * Math.acos(p / n));

            // Compute the angle extent
            n = Math.sqrt((ux * ux + uy * uy) * (vx * vx + vy * vy));
            p = ux * vx + uy * vy;
            sign = (ux * vy - uy * vx < 0) ? -1d : 1d;
            double angleExtent = Math.toDegrees(sign * Math.acos(p / n));
            if (!sweepFlag && angleExtent > 0)
            {
                angleExtent -= 360f;
            }
            else if (sweepFlag && angleExtent < 0)
            {
                angleExtent += 360f;
            }

            angleExtent %= 360f;
            angleStart %= 360f;

            //
            // We can now build the resulting Arc2D in double precision
            //
            Arc2D.Double arc = new Arc2D.Double(
                cx - rx, cy - ry,
                rx * 2d, ry * 2d,
                -angleStart, -angleExtent,
                Arc2D.OPEN);

            // Create a path iterator of the rotated arc
            PathIterator i = arc.getPathIterator(
                AffineTransform.getRotateInstance(
                    angle, arc.getCenterX(), arc.getCenterY()));

            // Add the segments to the bezier path
            double[] coords = new double[6];
            i.next(); // skip first moveto
            while (!i.isDone())
            {
                int type = i.currentSegment(coords);
                switch (type)
                {
                    case PathIterator.SEG_CUBICTO:
                        curveTo(new Point2D.Double(coords[0], coords[1]), new Point2D.Double(coords[2], coords[3]),
                            new Point2D.Double(coords[4], coords[5]));
                        break;
                    case PathIterator.SEG_LINETO:
                        lineTo(new Point2D.Double(coords[0], coords[1]));
                        break;
                    case PathIterator.SEG_QUADTO:
                        quadTo(new Point2D.Double(coords[0], coords[1]), new Point2D.Double(coords[2], coords[3]));
                        break;
                }

                i.next();
            }
        }
        public int findSegment(Point2D.Double find, double tolerance) {
            // XXX - This works only for straight lines!
            Node v1, v2;
            BezierPath tempPath = new BezierPath();
            Node t1, t2;
            tempPath.add(t1 = new Node());
            tempPath.add(t2 = new Node());

            for (int i = 0, n = size() - 1; i < n; i++) {
                v1 = get(i);
                v2 = get(i + 1);
                if (v1.mask == 0 && v2.mask == 0) {
                    if (Geom.lineContainsPoint(v1.x[0], v1.y[0], v2.x[0], v2.y[0], find.x, find.y, tolerance)) {
                        return i;
                    }
                } else {
                    t1.setTo(v1);
                    t2.setTo(v2);
                    tempPath.invalidatePath();
                    if (tempPath.outlineContains(find, tolerance)) {
                        return i;
                    }
                }
            }
            if (isClosed && size() > 1) {
                v1 = get(size() - 1);
                v2 = get(0);
                if (v1.mask == 0 && v2.mask == 0) {
                    if (Geom.lineContainsPoint(v1.x[0], v1.y[0], v2.x[0], v2.y[0], find.x, find.y, tolerance)) {
                        return size() - 1;
                    }
                } else {
                    t1.setTo(v1);
                    t2.setTo(v2);
                    tempPath.invalidatePath();
                    if (tempPath.outlineContains(find, tolerance)) {
                        return size() - 1;
                    }
                }
            }
            return -1;
        }
    }
}


    