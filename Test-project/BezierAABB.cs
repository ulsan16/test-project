namespace Test_project
{
    public class BezierAABB {

    private double x1 = 0;
    private double x2 = 0;
    private double y1 = 0;
    private double y2 = 0;
    private int bezierPathSize = 0;
    private bool isBezierPathClosed;
    private BezierPath bezierPath;

    
    public BezierAABB(BezierPath bezierPath) {
        this.bezierPath = bezierPath;
        this.bezierPathSize = bezierPath.size();
        this.isBezierPathClosed = bezierPath.isClosed();

        if (this.bezierPathSize > 0) {
            this.x1 = this.x2 = bezierPath.get(0).x[0];
            this.y1 = this.y2 = bezierPath.get(0).y[0];
            computeBezierAABB();
            
        }
    }
    
    public Rectangle2 generate2DRectangle() {
        return new Rectangle2D(x1, y1, x2 - x1, y2 - y1);
    }

    private void computeBezierAABB() {
        this.handleFirstNode();
        this.handleLastNode();
        this.handleIntermediateNodes();
    }

    private void handleFirstNode() {
        Node node = bezierPath.get(0);
        if (isBezierPathClosed) {
            checkC1_Mask(node);
        }
        checkC2_Mask(node);
    }

    private void handleLastNode() {
        Node node = bezierPath.get(bezierPathSize - 1);
        updateBounds(node.y[0], node.x[0]);
        checkC1_Mask(node);
        if (isBezierPathClosed) {
            checkC2_Mask(node);
        }
    }

    private void handleIntermediateNodes() {
        for (int i = 1, n = bezierPathSize - 1; i < n; i++) {
            Node node = bezierPath.get(i);
            updateBounds(node.y[0], node.x[0]);
            checkC1_Mask(node);
            checkC2_Mask(node);
        }
    }
    
    private void checkC1_Mask(Node node) {
        if ((node.mask & C1_MASK) != 0) {
            updateBounds(node.y[1], node.x[1]);
        }
    }
    
    private void checkC2_Mask(Node node) {
        if ((node.mask & C2_MASK) != 0) {
            updateBounds(node.y[2], node.x[2]);
        }
    }

    private void updateBounds(double y, double x) {
        if (x < x1) {
            x1 = x;
        }
        if (y < y1) {
            y1 = y;
        }
        if (x > x2) {
            x2 = x;
        }
        if (y > y2) {
            y2 = y;
        }
    }
}