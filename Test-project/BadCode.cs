using System.Threading.Tasks;

namespace Test_project
{
    public interface IBadCode
    {
        Rectangle2D getBounds2D();
    }
    
    public class Node
    {
        public int mask { get; set;}
        public int[] y { get; set;}
        public int[] x { get; set;}
    }

    public class Rectangle2D
    {
        public Rectangle2D(double x1, double x2, double y1, double y2)
        {
            
        }
    }

    public class BadCode : IBadCode
    {
        private bool isClosed;

        public Rectangle2D getBounds2D()
        {
            return (Rectangle2D) bounds == null
                ? new BezierAABB(this).generate2DRectangle()
                : (Rectangle2D) bounds.clone();
        }
    }
}